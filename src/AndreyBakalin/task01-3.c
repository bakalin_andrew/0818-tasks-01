#include <stdio.h>
#include <locale.h>

int main() {
	setlocale(LC_ALL, "rus");

	char str[20];
	long long int sum = 0, cur_num = 0, i;

	puts("������� ������");
	fgets(str, 20, stdin);
	str[strlen(str) - 1] = 0;

	for (i = 0; i < strlen(str); i++) {
		if ((str[i] >= '0') && (str[i] <= '9')) {
			cur_num *= 10;
			cur_num += str[i] - '0';
		}
		else {
			sum += cur_num;
			cur_num = 0;
		}
	}
	if (cur_num != 0)
		sum += cur_num;

	printf("%d", sum);

	return 0;
}